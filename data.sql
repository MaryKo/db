INSERT INTO Horse Values (1, 'Jo', 'arabian', 4, 'stallion', 170, 100, 200 );
INSERT INTO Horse Values (2, 'Buzifal', 'friz', 3, 'stallion', 175, 107, 100 );
INSERT INTO Horse Values (3, 'Kesh', 'brumby', 8, 'mare', 160, 120, 300 );
INSERT INTO Horse Values (4, 'Arra', 'french Trotter', 5, 'mare', 180, 150, 200 );
INSERT INTO Horse Values (5, 'Noi', 'arabian', 6, 'stallion', 162, 130, 120 );

INSERT INTO Hippodrome_type Values (1, 'racing','herbal');
INSERT INTO Hippodrome_type Values (2, 'combined','weatherproof');
INSERT INTO Hippodrome_type Values (3, 'running', 'ground');

INSERT INTO Hippodrome Values (1, 2, 'Idalgo', 'France', 'Paris', 700);

INSERT INTO Jockey Values (1, 'Bob', 'Dilan','man', 25, 170, 60, 100);
INSERT INTO Jockey Values (2, 'Jack', 'Danials','man', 28, 175, 63, 200);

INSERT INTO Parlay Values(1, 1, 'Win', 200);
INSERT INTO Parlay Values(2, 3, 'Against', 100);
INSERT INTO Parlay Values(3, 5, 'Place', 20);
INSERT INTO Parlay Values(4, 4, 'Across the board', 30);
INSERT INTO Parlay Values(5, 3, 'Show',300);
INSERT INTO Parlay Values(6, 2, 'Against',100);

INSERT INTO Stewart Values(1, 'Mark', 'Alan', 20, 2);
INSERT INTO Stewart Values(2, 'Kris', 'Mavil', 23, 5);
INSERT INTO Stewart Values(3, 'Jon', 'Risly', 19, 1);

INSERT INTO Judge Values(1, 'Mary', 'Wood', 30, 10);
INSERT INTO Judge Values(2, 'Nick', 'Cave', 28, 5);

INSERT INTO Race Values(1, 1, '2013-02-12', NULL, 2, 2, 'barrier jumps', true);
INSERT INTO Race Values(2, 1, '2013-03-02', '12:00:00', 2, 2, 'barrier jumps', true);

INSERT INTO Player Values(1, 'Bob', 'White', 2, 2, false, Null);
INSERT INTO Player Values(2, 'Bejamib', 'Button', 4, 1, true, 1000);

INSERT INTO Registration Values(1, 1, 4, '2012-01-01','13:00:00');
INSERT INTO Registration Values(2, 2, 4, '2012-02-05','15:50:00');
INSERT INTO Registration Values(3, 2, 1, '2013-01-02','16:20:00');
INSERT INTO Registration Values(4, 2, 2, '2012-06-09','11:08:00');
INSERT INTO Registration Values(5, 2, 3, '2012-05-21','12:01:00');
INSERT INTO Registration Values(6, 1, 1, '2012-01-01','10:01:00');







	