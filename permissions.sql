CREATE ROLE dba WITH SUPERUSER CREATEROLE CREATEDB LOGIN;
CREATE ROLE manager WITH LOGIN;
CREATE ROLE client WITH LOGIN;
CREATE ROLE guest WITH NOLOGIN;

GRANT CONNECT ON DATABASE hippodrome TO manager;
GRANT CONNECT ON DATABASE hippodrome TO client;

-- dba �� ��������� � ������, �.�. �� ���������

-- ������ ��� ���������
GRANT SELECT ON ALL TABLES IN SCHEMA public TO manager;
GRANT INSERT, UPDATE, DELETE ON Player, Parlay, Registration TO manager;

-- ������ ��� �������
GRANT SELECT (parley_id, player_id, race_id, parley_style) ON Parlay TO client;
GRANT SELECT ON Horse, Hippodrome, Jockey, Player, Stewart, Judge, Race TO client;

-- ������ ��� �����
GRANT SELECT ON Horse, Hippodrome, Jockey, Stewart, Judge, Race TO guest;


