create or replace function random_string(length integer) returns text as 
$$
declare
  chars text[] := '{A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
  result text := '';
  i integer := 0;
begin
  if length < 0 then
    raise exception 'Given length cannot be less than 0';
  end if;
  for i in 1..length loop
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  end loop;
  return result;
end;
$$ language plpgsql;

--��� ������
create or replace function random_horse(num_pl integer) returns integer as
$$
declare 
num integer := num_pl;
begin
	LOOP
	INSERT INTO Horse Values (
	num,
	random_string(10),
	random_string(10),
	round(random()*15),
	'stallion',
	round(random()*200)+1,
	round(random()*300)+1,
	round(random()*400)+1 
	);
	num:= num +1;
    EXIT WHEN num > 10000;
	END LOOP;
	return num;
end;
$$ language plpgsql;

--��� ���������
create or replace function random_hippodrome(num_pl integer) returns integer as
$$
declare 
num integer := num_pl;
begin
	LOOP
	INSERT INTO Hippodrome Values (
	num,
	'racing',
	'herbal',
	random_string(10),
	random_string(10),
	random_string(10),
	round(random()*500) +500
	);
	num:= num +1;
    EXIT WHEN num > 10000;
	END LOOP;
	return num;
end;
$$ language plpgsql;

--��� jockey
create or replace function random_jockey(num_pl integer) returns integer as
$$
declare 
num integer := num_pl;
begin
	LOOP
	INSERT INTO Jockey Values (
	num,
	random_string(10),
	random_string(10),
	'man',
	'1990-01-01',
	round(random()*30) + 150,
	round(random()*20) + 45,
	round(random()*400) +1
	);
	num:= num +1;
    EXIT WHEN num > 10000;
	END LOOP;
	return num;
end;
$$ language plpgsql;

-- ��� ������
create or replace function random_player(num_pl integer) returns integer as
$$
declare 
num integer := num_pl;
begin
	LOOP
	INSERT INTO Player Values(
	num,
	random_string(10),
	random_string(10)
	);
	num:= num +1;
    EXIT WHEN num > 10000;
	END LOOP;
	return num;
end;
$$ language plpgsql;

--��� stewart

create or replace function random_stewart(num_pl integer) returns integer as
$$
declare 
num integer := num_pl;
begin
	LOOP
	INSERT INTO Stewart Values(
	num,
	random_string(10),
	random_string(10),
	'1989-01-01',
	round(random()*8) +1
	);
	num:= num +1;
    EXIT WHEN num > 10000;
	END LOOP;
	return num;
end;
$$ language plpgsql;

--��� Judge

create or replace function random_judge(num_pl integer) returns integer as
$$
declare 
num integer := num_pl;
begin
	LOOP
	INSERT INTO Judge Values(
	num,
	random_string(10),
	random_string(10),
	'1989-01-01',
	round(random()*8) +1
	);
	num:= num +1;
    EXIT WHEN num > 10000;
	END LOOP;
	return num;
end;
$$ language plpgsql;

--��� race
create or replace function race(num_pl integer) returns integer as
$$
declare 
num integer := num_pl;
begin
	LOOP
	INSERT INTO Race Values(
	num,
	round(random()*1000) +1, --hippodrome_id 
	NULL,
	NULL,
	round(random()*1000) +1,
	round(random()*1000) +1,
	'barrier jumps',
	true,
	true
	);
	num:= num +1;
    EXIT WHEN num > 10000;
	END LOOP;
	return num;
end;
$$ language plpgsql;

-- parley
create or replace function random_parley(num_pl integer) returns integer as
$$
declare 
num integer := num_pl;
begin
	LOOP
	INSERT INTO Parlay Values(
	num,
	round(random()*1000) +1,
	round(random()*1000) +1,
	round(random()*1000) +1,
	'Against',
	round(random()*800)+1
	);
	num:= num +1;
    EXIT WHEN num > 10000;
	END LOOP;
	return num;
end;
$$ language plpgsql;

-- registration

create or replace function random_registration(num_pl integer) returns integer as
$$
declare 
num integer := num_pl;
begin
	LOOP
	INSERT INTO Registration Values(
	num,
	round(random()*1000) +1,
	round(random()*1000) +1,
	NULL,
	NULL
	);
	num:= num +1;
    EXIT WHEN num > 10000;
	END LOOP;
	return num;
end;
$$ language plpgsql;