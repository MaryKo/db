CREATE TABLE Horse (
	horse_id integer NOT NULL, --#1
	name varchar(25) NOT NULL, --#2
	breed_of_horse varchar(25),
	age integer,
	sex varchar(25)
	CHECK (sex IN('stallion', 'mare', 'gelding')), --#3
	height real 
	CHECK (height > 0), --#4
	weight real 
	CHECK (weight > 0), --#5
	rating real
	CHECK (rating > 0), --#6
	PRIMARY KEY (horse_id) --#7
);

CREATE TABLE Hippodrome_type(
	hippodrome_type_id integer NOT NULL, --#8
	style varchar(25) NOT NULL --#9
	CHECK (style IN ('racing','running','combined')), --#10
	covering_tracks varchar(25) NOT NULL --#11
	CHECK (covering_tracks IN ('herbal','ground','weatherproof')), --#12
	PRIMARY KEY (hippodrome_type_id) --#13
);

CREATE TABLE Hippodrome(
	hippodrome_id integer NOT NULL, --#14
	hippodrome_type_id integer NOT NULL, --#15
	name varchar(25) NOT NULL, --#16
	country varchar(25) NOT NULL, --#17
	city varchar(25) NOT NULL, --#18
	square real NOT NULL --#19
	CHECK (square > 0), --#20
	PRIMARY KEY (hippodrome_id), --#21
	FOREIGN KEY (hippodrome_type_id) REFERENCES Hippodrome_type(hippodrome_type_id) --#22
);
	
CREATE TABLE Jockey (
	jockey_id integer NOT NULL, --#23
	name varchar(25) NOT NULL, --#24
	surname varchar(25) NOT NULL, --#25
	sex varchar(25) NOT NULL --#26
	CHECK (sex IN ('man', 'woman')), --#27
	age integer NOT NULL --#28
	CHECK (age > 18), --#29
	height real 
	CHECK (height > 0), --#30
	weight real 
	CHECK (weight > 0), --#31
	rating real
	CHECK (rating > 0), --#32
	PRIMARY KEY (jockey_id) --#33
);

CREATE TABLE Parlay(
	parley_id integer NOT NULL, --#34
	horse_id integer NOT NULL, --#35
	parley_style varchar(25) NOT NULL --#36 
	CHECK (parley_style IN ('Win', 'Against','Place', 'Show', 'Across the board')), --#37
	parley_size real NOT NULL --#38
	CHECK (parley_size > 0), --#39
	PRIMARY KEY (parley_id),--#40
	FOREIGN KEY (horse_id) REFERENCES Horse(horse_id) --#41
);	

CREATE TABLE Stewart (
	stewart_id integer NOT NULL, --#42
	name varchar(25) NOT NULL, --#43
	surname varchar(25) NOT NULL, --#44
	age integer NOT NULL --#45
	CHECK (age > 18), --#46
	experience integer,
	PRIMARY KEY (stewart_id) --#47
);

CREATE TABLE Judge (
	judge_id integer NOT NULL,--#48
	name varchar(25) NOT NULL, --#49
	surname varchar(25) NOT NULL, --#50
	age integer NOT NULL --#51
	CHECK (age > 18), --#52
	experience integer,
	PRIMARY KEY (judge_id) --#53
);

CREATE TABLE Race(
	race_id integer NOT NULL, --#54
	hippodrome_id integer NOT NULL, --#55
	race_date date,
	race_time time,
	stewart_id integer,
	judge_id integer,
	race_style varchar(30)
	CHECK (race_style IN ('flat race','barrier jumps','trotter race')),--#56
	fixed_weight_category boolean, 
	PRIMARY KEY (race_id), --#57
	FOREIGN KEY (hippodrome_id) REFERENCES Hippodrome(hippodrome_id), --#58
	FOREIGN KEY (stewart_id) REFERENCES Stewart(stewart_id),--#59
	FOREIGN KEY (judge_id) REFERENCES Judge(judge_id) --#60
);

CREATE TABLE Player(
	player_id integer NOT NULL, --#61
	name varchar(25),
	surname varchar(25),
	parley_id integer NOT NULL, --#62
	race_id integer NOT NULL, --#63
	win boolean, 
	win_size real
	CHECK (win_size >= 0), --#64
	PRIMARY KEY (player_id), --#65
	FOREIGN KEY (parley_id) REFERENCES Parlay(parley_id), --#66
	FOREIGN KEY (race_id) REFERENCES Race(race_id) --#67
);

CREATE TABLE Registration(
	registration_id integer NOT NULL, --#68
	race_id integer NOT NULL, --#69
	horse_id integer NOT NULL, --#70
	registration_date date,
	registration_time time,
	PRIMARY KEY (registration_id), --#71
	FOREIGN KEY (race_id) REFERENCES Race(race_id), --#72
	FOREIGN KEY (horse_id) REFERENCES Horse(horse_id) --#73
);