������ ����������:

1. ����������� � ����� ������, ������ ������ � ������ � ����.
INSERT INTO Race Values(2, 1, '2013-03-02', '12:00:00', 2, 2, 'barrier jumps', true, true);

2. �� ����������� ��������� ����� ������, ������� ������ � ������ � ������� �����������.
3. ����������� ������ ������. ������ ������ � ����.
4. ����������� ����� ������.
5. ����� ��������� ������ ��������� ��� ����������� (availability = FALSE).
UPDATE Race SET availability = false WHERE race_id = 2;

6. ����������� ���-�� �������� ������� - ��������� ���� win � win_size ������� Parlay.
UPDATE Parlay SET win = true, win_size = 200 WHERE parley_id = 1;

7. ������ ������ � ���� ��������� �� ������ ������ �������.
SELECT * FROM Race WHERE availability = true;

8. ������ � ���, ����� ������ ������� ������.
SELECT * FROM Player 
	INNER JOIN Parlay 
	ON Player.player_id = Parlay.player_id;
	
9. ������ � ���, ����� ������ � ����� ������ ���������.
SELECT Horse.horse_id, Horse.name, Registration. race_id
FROM Horse 
	LEFT OUTER JOIN Registration
	ON Horse.horse_id = Registration.horse_id;

10. ������� ���������� ������� � ������ ������.
SELECT Registration.race_id, COUNT ( DISTINCT Registration.horse_id )  FROM Registration
	GROUP BY Registration.race_id;

11. ������� ���������� �������, � ������� ��������� ������ ������.
���������� �10.

12. ����� ���� �������, ����� ����� ������ ������� ������\����� 100.
SELECT Parlay.player_id, SUM (Parlay.parley_size) FROM Parlay
	GROUP BY Parlay.player_id
	HAVING SUM (Parlay.parley_size) >= 100;
	
13. ������ �� ���� �������, ��������������� �� ��������.
SELECT Horse.rating, Horse.horse_id, Horse.name FROM Horse
	ORDER BY 1 DESC;
	
14. ������ �� ���� ������, ��������������� �� ��������.
���������� �13.

15. ������� ������ ���� ������� ������, ����� ������ �� ��������� � �������, � ����� ���.
SELECT Horse.horse_id, Horse.name, 'take part'
FROM Horse 
	WHERE Horse.horse_id =  ANY(
	SELECT Registration.horse_id FROM Registration)

UNION

SELECT Horse.horse_id, Horse.name, 'not take part'
FROM Horse 
	WHERE NOT Horse.horse_id =  ANY(
	SELECT Registration.horse_id FROM Registration)
ORDER BY 1;



 