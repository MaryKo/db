-- ����������� ��������� ����������� (���������� ���� �� �.�. �2)
-- ��� ������� �����������:
CREATE TABLE Registration(
	registration_id serial NOT NULL, --#68
	race_id serial NOT NULL, --#69
	horse_id serial NOT NULL, --#70
	registration_date date,
	registration_time time,
	PRIMARY KEY (registration_id), --#71
	FOREIGN KEY (race_id) REFERENCES Race(race_id), --#72
	FOREIGN KEY (horse_id) REFERENCES Horse(horse_id) --#73
);

-- ���������� �������
CREATE OR REPLACE FUNCTION tr_reg_func() RETURNS trigger AS $tr_reg$
BEGIN
-- �������� ������� �����������
IF NEW.registration_id IS NULL THEN
		RAISE EXCEPTION 'registration_id cannot be NULL';
	END IF;
IF NEW.race_id  IS NULL THEN
		RAISE EXCEPTION 'race_id cannot be NULL';
	END IF;
IF NEW.horse_id IS NULL THEN
		RAISE EXCEPTION 'horse_id cannot be NULL';
	END IF;
-- �������� ������������� ��������������� ������� ������	
IF (SELECT race_id FROM Race WHERE race_id = NEW.race_id) IS NULL THEN
		RAISE EXCEPTION 'foreign key race_id does not exist';
	END IF;
IF (SELECT horse_id FROM Horse WHERE horse_id = NEW.horse_id) IS NULL THEN
		RAISE EXCEPTION 'foreign key horse_id does not exist';
	END IF;
RETURN NEW;
END;
$tr_reg$ LANGUAGE plpgsql;

-- �������, ���������� �������
CREATE TRIGGER reg_trigger BEFORE INSERT OR UPDATE
	ON Registration FOR EACH ROW
	EXECUTE PROCEDURE tr_reg_func();

-- �����������, ������������� ��������� ������� ����� ��� ���������� ������ (�.�. �3 ��. 1.3 ��� 1.2)
-- ������� ����� ��������� ������������ ��� ������� Stewart:
CREATE TABLE Stewart (
	stewart_id serial NOT NULL, --#42
	name varchar(25) NOT NULL, --#43
	surname varchar(25) NOT NULL, --#44
	date_of_b_day date NOT NULL, --#45
	experience integer,
	PRIMARY KEY (stewart_id) --#47
);

-- ���������� �������
CREATE OR REPLACE FUNCTION tr_stewart_func() RETURNS trigger AS $tr_stewart$
BEGIN
-- �������� ������� �����������
IF NEW.stewart_id IS NULL THEN
		RAISE EXCEPTION 'stewart_id cannot be NULL';
	END IF;
IF NEW.name IS NULL THEN
		RAISE EXCEPTION 'name cannot be NULL';
	END IF;
IF NEW.surname IS NULL THEN
		RAISE EXCEPTION 'surname cannot be NULL';
	END IF;
IF NEW.date_of_b_day IS NULL THEN
		RAISE EXCEPTION 'date_of_b_day cannot be NULL';
	END IF;
RETURN NEW;
END;
$tr_stewart$ LANGUAGE plpgsql;

-- �������, ���������� �������
CREATE TRIGGER stewart_trigger BEFORE INSERT OR UPDATE
	ON Stewart FOR EACH ROW
	EXECUTE PROCEDURE tr_stewart_func();
	
-- �������������� ��������� ��� ����� �� ������
-- �������� �������������� ��� ������� Registration:

CREATE TABLE Registration(
	registration_id serial NOT NULL, --#68
	race_id serial NOT NULL, --#69
	horse_id serial NOT NULL, --#70
	registration_date date,
	registration_time time,
	PRIMARY KEY (registration_id), --#71
	FOREIGN KEY (race_id) REFERENCES Race(race_id), --#72
	FOREIGN KEY (horse_id) REFERENCES Horse(horse_id) --#73
);

CREATE TABLE Registration_audit
(
    operation         char(6)   NOT NULL,
    stamp             timestamp NOT NULL,
    user_id           text      NOT NULL,
    registration_id   serial 	NOT NULL,
	race_id serial 				NOT NULL, 
	horse_id serial 			NOT NULL,
	registration_date date,
	registration_time time
);

CREATE OR REPLACE FUNCTION tr_audit_func() RETURNS TRIGGER AS $audit_tr$
BEGIN
        
IF (TG_OP = 'DELETE') THEN
    INSERT INTO Registration_audit SELECT 'DELETE', now(), user, OLD.*;
    RETURN OLD;
ELSIF (TG_OP = 'UPDATE') THEN
    INSERT INTO Registration_audit SELECT 'UPDATE', now(), user, NEW.*;
    RETURN NEW;
ELSIF (TG_OP = 'INSERT') THEN
    INSERT INTO Registration_audit SELECT 'INSERT', now(), user, NEW.*;
    RETURN NEW;
    END IF;
END;
$audit_tr$ LANGUAGE plpgsql;

CREATE TRIGGER audit_trigger
AFTER INSERT OR UPDATE OR DELETE 
	ON Registration FOR EACH ROW 
	EXECUTE PROCEDURE tr_audit_func();
	
-- �������������� ���������� ��������� (�����������) ����(-��) (�������� INSERT/UPDATE/DELETE)
-- �������� ������� � ������������ ������
CREATE TABLE Results(
	result_id serial NOT NULL,
	race_id serial NOT NULL,
	win_horse_id serial NOT NULL,
	PRIMARY KEY (result_id), 
	FOREIGN KEY (race_id) REFERENCES Race(race_id), 
	FOREIGN KEY (win_horse_id) REFERENCES Horse(horse_id) 
);

-- ��� ���������� ������ � ������� �����������(�.�. ������ - ���������� � ������������ ������), 
-- ����������� ������� ���� ������ �� 10
CREATE OR REPLACE FUNCTION tr_horse_rating_func() RETURNS TRIGGER AS $hor_tr$
BEGIN
UPDATE Horse SET rating = rating + 10 WHERE Horse.horse_id = NEW.win_horse_id;
RETURN NEW;       
END;
$hor_tr$ LANGUAGE plpgsql;

CREATE TRIGGER horse_rating_trigger
AFTER INSERT  
	ON Results FOR EACH ROW 
	EXECUTE PROCEDURE tr_horse_rating_func();



