-- �������� ������������� ��� ����������� ������ Parlay � Horse, � ���������� ������� 
-- ������ �������, �� ������� ������� ������ (� ������� ������������� �� �������), �.�.
-- ��� ������������� 2 ������, � ������������ �� ��� ������� ����� ������, �������������
-- ������� �� ��������

CREATE VIEW horse_with_parley
	AS SELECT a.horse_id, a.name, b.parley_id, b.race_id, player_id, b.parley_style, b.parley_size
		FROM Horse a, Parlay b
		WHERE a.horse_id = b.horse_id;
		
--������� ����������� �������:
GRANT SELECT ON horse_with_parley TO manager;

-- ��������� ��� ������������� �������� INSERT/UPDATE/DELETE
-- Insert

CREATE RULE horse_with_parley_insert AS ON INSERT TO horse_with_parley
        DO INSTEAD (
        INSERT INTO Horse (horse_id, name)
        VALUES (NEW.horse_id, NEW.name);                    

        INSERT INTO Parlay (parley_id, horse_id, player_id,	race_id , parley_style,	parley_size)
        VALUES (NEW.parley_id, NEW.horse_id, NEW.player_id,	NEW.race_id , NEW.parley_style,	NEW.parley_size)
        );

--Update

CREATE OR REPLACE RULE horse_with_parley_update AS ON UPDATE TO horse_with_parley
        DO INSTEAD (
        UPDATE Horse SET 
			name = NEW.name
            WHERE  horse_id=OLD.horse_id;
                
        UPDATE Parlay SET
            horse_id = NEW.horse_id, player_id = NEW.player_id,	race_id = NEW.race_id, parley_style = NEW.parley_style, parley_size = NEW.parley_size
			WHERE parley_id=OLD.parley_id;
		);

--Delete
		
CREATE OR REPLACE RULE horse_with_parley_delete AS ON DELETE TO horse_with_parley
        DO INSTEAD (
        DELETE FROM Horse WHERE horse_id=OLD.horse_id; 
        DELETE FROM Parlay WHERE parley_id=OLD.parley_id;
        );

--- CREATE TRIGGER
CREATE OR REPLACE FUNCTION horse_with_parley_func()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $function$
   BEGIN
      IF TG_OP = 'INSERT' THEN
        INSERT INTO Horse (horse_id, name)
        VALUES (NEW.horse_id, NEW.name);                 
        INSERT INTO Parlay (parley_id, horse_id, player_id,	race_id , parley_style,	parley_size)
        VALUES (NEW.parley_id, NEW.horse_id, NEW.player_id,	NEW.race_id , NEW.parley_style,	NEW.parley_size);
        RETURN NEW;
		
      ELSIF TG_OP = 'UPDATE' THEN
        UPDATE Horse SET 
			name = NEW.name
            WHERE  horse_id=OLD.horse_id;
        UPDATE Parlay SET
            horse_id = NEW.horse_id, player_id = NEW.player_id,	race_id = NEW.race_id, parley_style = NEW.parley_style, parley_size = NEW.parley_size
			WHERE parley_id=OLD.parley_id;
		RETURN NEW;
		
      ELSIF TG_OP = 'DELETE' THEN
        DELETE FROM Horse WHERE horse_id=OLD.horse_id; 
        DELETE FROM Parlay WHERE parley_id=OLD.parley_id;
        RETURN NULL;
      END IF;
      RETURN NEW;
    END;
$function$;

CREATE TRIGGER horse_with_parley_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON
      horse_with_parley FOR EACH ROW EXECUTE PROCEDURE horse_with_parley_func();
